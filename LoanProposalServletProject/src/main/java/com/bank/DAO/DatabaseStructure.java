package com.bank.DAO;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import com.bank.constants.LoanConstants;

public class DatabaseStructure {

		public static void createTables()
		{
			createEmployeeTable();
			createCollateralTable();
			createCustomerTable();
			createLoanTable();
			createLoanCollateralTable();
			insertDataInCollateralTable();
			insertDataInEmployeeTable();
		}
	
		public static void createEmployeeTable() {
			Connection conn = DBConnection.getConn();
			String sql =	
					"CREATE TABLE IF NOT EXISTS EMPLOYEE" +
					"(employeeId VARCHAR(255) NOT NULL, " +
					"employeeName VARCHAR(255), PRIMARY KEY(employeeId))"; 
			try {
				Statement stmt = conn.createStatement();
				stmt.executeUpdate(sql);
				System.out.println("Created EMPLOYEE table in given database...");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		public static void insertDataInEmployeeTable() {
			Connection conn = DBConnection.getConn();
			String sql =	
					"INSERT INTO EMPLOYEE VALUES('EMP001','Alan George'),"
					+"('EMP002','Sanvi Mehta'),"
					+"('EMP003','Ryan Armstrong')"; 
			try {
				Statement stmt = conn.createStatement();
				stmt.executeUpdate(sql);
				System.out.println("INSERTED DATA FOR EMPLOYEES..");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		public static void createCollateralTable() {
			Connection conn = DBConnection.getConn();
			String sql =	
					"CREATE TABLE IF NOT EXISTS COLLATERAL" +
					"(collateralId VARCHAR(255) NOT NULL, " +
					"collateralType VARCHAR(255), PRIMARY KEY(collateralId))"; 
			try {
				Statement stmt = conn.createStatement();
				stmt.executeUpdate(sql);
				System.out.println("Created COLLATERAL table in given database...");
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		public static void insertDataInCollateralTable() {
			Connection conn = DBConnection.getConn();
			String sql =	
					"INSERT INTO COLLATERAL VALUES('C001','VEHICLE REGISTRATION'),"
					+"('C002','HOME TITLE'),"
					+"('C003','INSURANCE DOCUMENTS'),"
					+"('C004','FIXED DEPOSITS')"; 
			try {
				Statement stmt = conn.createStatement();
				stmt.executeUpdate(sql);
				System.out.println("INSERTED DATA FOR COLLATERALS..");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		public static void createCustomerTable() {
			Connection conn = DBConnection.getConn();
			String sql =	
					"CREATE TABLE IF NOT EXISTS CUSTOMER" +
					"(customerEmailId VARCHAR(255) NOT NULL, " +
					"customerName VARCHAR(255), "
					+ "customerAddress VARCHAR(255),"
					+"customerIdentity VARCHAR(255),"
					+"annualIncome DOUBLE,"
					+"incomeTaxReturnAttached BOOLEAN,"
					+ "PRIMARY KEY(customerEmailId))"; 
			try {
				Statement stmt = conn.createStatement();
				stmt.executeUpdate(sql);
				System.out.println("Created table CUSTOMER in given database...");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		public static void createLoanTable() {
			Connection conn = DBConnection.getConn();
			String sql =	
					"CREATE TABLE IF NOT EXISTS LOAN" +
					"(loanId VARCHAR(255) NOT NULL, "
					+"loanType VARCHAR(255), "
					+"loanAmount DOUBLE,"
					+"interestRate DOUBLE,"
					+"period DOUBLE,"
					+"isApproved BOOLEAN,"
					+"Customer_Email VARCHAR(255),"
					+ "employeeId VARCHAR(255),"
					+ "PRIMARY KEY(loanId),"
					+ "FOREIGN KEY (Customer_Email)" + 
					" REFERENCES customer(customerEmailId),"
					+ "FOREIGN KEY (employeeId)" + 
					" REFERENCES employee(employeeId)"
					+ ")"; 
			System.out.println(sql);
			try {
				Statement stmt = conn.createStatement();
				stmt.executeUpdate(sql);
				System.out.println("Created table LOAN in given database...");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		public static void createLoanCollateralTable()
		{
			Connection conn = DBConnection.getConn();
			String sql =	
					"CREATE TABLE IF NOT EXISTS " + LoanConstants.LOANCOLLATERALTABLE+
					" (loanid VARCHAR(255) NOT NULL, " +
					"collateralId VARCHAR(255), "
					+ "PRIMARY KEY(loanid, collateralId),"
					+"FOREIGN KEY (loanid) REFERENCES LOAN(loanid),"
					+"FOREIGN KEY (collateralId) REFERENCES COLLATERAL(collateralId)"
					+ ")";
			System.out.println(sql);
			try {
				Statement stmt = conn.createStatement();
				stmt.executeUpdate(sql);
				System.out.println("Created table LOAN_COLLATERAl in given database...");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
}
