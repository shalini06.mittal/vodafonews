package com.bank.DAO;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.bank.constants.LoanConstants;
import com.bank.model.Collateral;
import com.bank.model.Employee;

public class EmployeeDAO {
	
	private static ArrayList<Employee> employees = fetchEmployees();
	
	public static ArrayList<Employee> getEmployees() {
		return employees;
	}

	private static  ArrayList<Employee> fetchEmployees()
	{
		System.out.println("fetching employees");
		ArrayList<Employee> emps = new ArrayList<Employee>();
		Connection conn = DBConnection.getConn();
		try {
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery("select * from employee");
			while(rs.next())
			{
				Employee emp = new Employee();
				emp.setEmployeeId(rs.getString(1));
				emp.setEmployeeName(rs.getString(2));
				emps.add(emp);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return emps;
	}
	public static ArrayList<Collateral> fetchCollaterals()
	{
		ArrayList<Collateral> collaterals = new ArrayList();
		Connection conn = DBConnection.getConn();
		try {
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery("select * from "+LoanConstants.COLLATERALTABLE);
			while(rs.next())
			{
				Collateral collateral = new Collateral();
				collateral.setCollateralId(rs.getString(1));
				collateral.setCollateralType(rs.getString(2));
				collaterals.add(collateral);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return collaterals;
	}
	
}
