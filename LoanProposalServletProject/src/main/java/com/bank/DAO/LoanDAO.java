package com.bank.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.bank.constants.LoanConstants;
import com.bank.model.Collateral;
import com.bank.model.Customer;
import com.bank.model.Employee;
import com.bank.model.Loan;

public class LoanDAO {

	public static boolean insertLoanProposal(Loan loan)
	{
		Connection conn = DBConnection.getConn();
		String sql = "insert into loan values(?,?,?,?,?,?, ?,?,?)";
		PreparedStatement st;
		try {
			st = conn.prepareStatement(sql);
			st.setString(1, loan.getLoanId());
			st.setString(2, loan.getLoanType());
			st.setDouble(3, loan.getLoanAmount());
			st.setDouble(4, loan.getInterestRate());
			st.setDouble(5, loan.getPeriod());
			st.setBoolean(6, loan.isApproved());
			st.setString(7, loan.getCustomer().getCustomerEmailId());
			st.setString(8, loan.getEmployee().getEmployeeId());
			st.setString(9, loan.getRemarks());
			st.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}

		return true;
	}
	public static boolean insertLoanCollateral(String loanid, String collateralid)
	{
		Connection conn = DBConnection.getConn();
		String sql = "insert into "+LoanConstants.LOANCOLLATERALTABLE+" values(?,?)";
		PreparedStatement st;
		try {
			st = conn.prepareStatement(sql);
			st.setString(1, loanid);
			st.setString(2, collateralid);
			st.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}

		return true;
	}
	public static Loan findLoanById(String loanid)
	{
		Loan loan = null;
		Connection conn = DBConnection.getConn();
		String sql = "SELECT * FROM Loan where loanid=?";
		PreparedStatement st;
		try {
			st = conn.prepareStatement(sql);
			st.setString(1, loanid);
			ResultSet rs = st.executeQuery();
			if(rs.next()) {
				loan = new Loan();
				loan.setLoanId(loanid);
				loan.setLoanType(rs.getString(2));
				loan.setLoanAmount(rs.getDouble(3));
				loan.setInterestRate(rs.getDouble(4));
				loan.setPeriod(rs.getDouble(5));
				loan.setApproved(rs.getBoolean(6));
				Customer customer = new Customer();
				customer.setCustomerEmailId(rs.getString(7));
				loan.setCustomer(customer);
				Employee employee = new Employee();
				employee.setEmployeeId(rs.getString(8));
				loan.setEmployee(employee);
				loan.setRemarks(rs.getString(9));
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		return loan;
	}
	public static void updateLoanStatus(boolean isApproved, String remarks, String loanid) {
		Connection conn = DBConnection.getConn();
		String sql = "update loan set isApproved=?, remarks=? where loanId =?";
		PreparedStatement st;
		try {
			st = conn.prepareStatement(sql);
			st.setBoolean(1, isApproved);
			st.setString(2, remarks);
			st.setString(3, loanid);
			st.executeUpdate();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
	}
	public static ArrayList<Loan> findLoanByEmployeeId(String empid)
	{
		ArrayList<Loan> loans = new ArrayList<Loan>();
		Connection conn = DBConnection.getConn();
		String sql = "SELECT l.*, lc.collateralId, c.collateralType "
				+ "FROM Loan l left join LOAN_COLLATERAL lc "
				+ "on l.loanId=lc.loanid \n" + 
				"left join COLLATERAL c on lc.collateralId=c.collateralId\n" + 
				"where l.employeeId=?";
		PreparedStatement st;
		try {
			st = conn.prepareStatement(sql);
			st.setString(1, empid);
			ResultSet rs = st.executeQuery();
			Loan loan = null;
			while(rs.next()) {
				if(loan == null) {
					loan = createNewLoan(rs);
				}
				else if(loan.getLoanId().equals(rs.getString(1))) {
					String collateralid = rs.getString(10);
					String collateraltype = rs.getString(11);
					loan.getCollaterals().add(new Collateral(collateralid, collateraltype));
				}
				else {
					loans.add(loan);
					loan = createNewLoan(rs);
				}
			}
			if(loan != null)
				loans.add(loan);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		return loans;
	}
	public static ArrayList<Loan> findLoanByCustomerId(String custid)
	{
		ArrayList<Loan> loans = new ArrayList<Loan>();
		Connection conn = DBConnection.getConn();

		String sql = "SELECT l.*, lc.collateralId, c.collateralType "
				+ "FROM Loan l left join LOAN_COLLATERAL lc "
				+ "on l.loanId=lc.loanid \n" + 
				"left join COLLATERAL c on lc.collateralId=c.collateralId\n" + 
				"where l.Customer_Email=?";
		PreparedStatement st;
		try {
			st = conn.prepareStatement(sql);
			st.setString(1, custid);
			ResultSet rs = st.executeQuery();
			Loan loan = null;
			while(rs.next()) {
				if(loan == null) {
					loan = createNewLoan(rs);
				}
				else if(loan.getLoanId().equals(rs.getString(1))) {
					String collateralid = rs.getString(10);
					String collateraltype = rs.getString(11);
					loan.getCollaterals().add(new Collateral(collateralid, collateraltype));
				}
				else {
					loans.add(loan);
					loan = createNewLoan(rs);
				}
			}
			if(loan!=null)
				loans.add(loan);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		return loans;
	}
	public static Loan createNewLoan(ResultSet rs) throws SQLException
	{
		Loan loan = new Loan();
		loan.setLoanId(rs.getString(1));
		loan.setLoanType(rs.getString(2));
		loan.setLoanAmount(rs.getDouble(3));
		loan.setInterestRate(rs.getDouble(4));
		loan.setPeriod(rs.getDouble(5));
		loan.setApproved(rs.getBoolean(6));
		Customer customer = new Customer();
		customer.setCustomerEmailId(rs.getString(7));
		loan.setCustomer(customer);
		Employee employee = new Employee();
		employee.setEmployeeId(rs.getString(8));
		loan.setEmployee(employee);	
		String collateralid = rs.getString(10);
		String collateraltype = rs.getString(11);
		if(!(collateralid==null))
			if(loan.getCollaterals() == null) {
				ArrayList<Collateral> list = new ArrayList<Collateral>();
				list.add(new Collateral(collateralid, collateraltype));
				loan.setCollaterals(list);
			}else
				loan.getCollaterals().add(new Collateral(collateralid, collateraltype));
		return loan;
	}

	public static int getLoanProposalCount()
	{
		Connection conn = DBConnection.getConn();
		int count = 0;
		try {
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery("select count(*) from loan");
			rs.next();
			count = rs.getInt(1);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return count;
	}
}
