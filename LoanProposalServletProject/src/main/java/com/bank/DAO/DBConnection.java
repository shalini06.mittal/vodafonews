package com.bank.DAO;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DBConnection{
	private static Connection conn = getConnection();

	public static Connection getConn() {
		return conn;
	}

	private static Connection getConnection()
	{
		
		Connection con = null;
		//FileInputStream inputStream = null;
		Properties prop = new Properties();
		InputStream propertiesInputStream = null;
		try {


			propertiesInputStream = DBConnection.class.getClassLoader().getResourceAsStream("/db.properties");
			System.out.println(propertiesInputStream);
			prop.load(propertiesInputStream);
			
			
			String driver = prop.getProperty("dbdriver");
			String url = prop.getProperty("dburl");
			String uname = prop.getProperty("dbuname");
			String pwd = prop.getProperty("dbpwd");
			Class.forName(driver);
			System.out.println("driver "+driver);
			con = DriverManager.getConnection(url,uname,pwd);
			System.out.println("connected");
		} catch (FileNotFoundException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			try {
				propertiesInputStream.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return con;
	}
	public static void closeConnection()
	{
		try {
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}