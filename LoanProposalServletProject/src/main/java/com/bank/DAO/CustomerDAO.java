package com.bank.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.bank.model.Customer;

public class CustomerDAO {

	public static boolean insertCustomer(Customer customer)
	{
		Connection conn = DBConnection.getConn();
		String sql = "insert into customer values(?,?,?,?,?,?)";
		PreparedStatement st;
		try {
			st = conn.prepareStatement(sql);
			st.setString(1, customer.getCustomerEmailId());
			st.setString(2, customer.getCustomerName());
			st.setString(3, customer.getCustomerAddress());
			st.setString(4, customer.getCustomerIdentity());
			st.setDouble(5, customer.getAnnualIncome());
			st.setBoolean(6, customer.isIncomeTaxReturnAttached());
			st.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}

		return true;
	}
	public static Customer findCustomerByEmail(String emailid)
	{
		Customer customer = null;
		Connection conn = DBConnection.getConn();
		String sql = "select * from customer where customerEmailId=?";
		PreparedStatement st;
		try {
			st = conn.prepareStatement(sql);
			st.setString(1, emailid);
			ResultSet rs = st.executeQuery();
			if(rs.next()) {
				customer = new Customer();
				customer.setCustomerEmailId(rs.getString(1));
				customer.setCustomerName(rs.getString(2));
				customer.setCustomerAddress(rs.getString(3));
				customer.setCustomerIdentity(rs.getString(4));
				customer.setAnnualIncome(rs.getDouble(5));
				customer.setIncomeTaxReturnAttached(rs.getBoolean(6));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		return customer;
	}

}
