package com.bank.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bank.DAO.CustomerDAO;
import com.bank.DAO.DatabaseStructure;
import com.bank.constants.LoanConstants;
import com.bank.model.Customer;

/**
 * Servlet implementation class RegisterServlet
 */
@WebServlet("/register")
public class RegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public RegisterServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//DatabaseStructure.createTables();
		RequestDispatcher dispatcher = request.getRequestDispatcher("register.jsp");
		request.setAttribute("types", LoanConstants.ID_TYPE);
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String customerName = request.getParameter("customerName");
		String customerAddress = request.getParameter("customerAddress");
		String customerIdentity = request.getParameter("customerIdentity");
		String customerEmailId = request.getParameter("customerEmailId");
		String password = request.getParameter("password");
		double annualIncome = Double.parseDouble(request.getParameter("annualIncome"));
		String attached =request.getParameter("incomeTaxReturnAttached");
		boolean incomeTaxReturnAttached = false;
		if(attached != null && attached.equals("on"))
			incomeTaxReturnAttached = true;
		System.out.println(customerEmailId);
		if(customerEmailId != null && !customerEmailId.isEmpty()) {
			Customer customer = new Customer(customerName, customerAddress, customerEmailId, customerIdentity, annualIncome, incomeTaxReturnAttached);
			if(CustomerDAO.insertCustomer(customer))
			{
				response.sendRedirect("login.jsp");
			}
			else {
				RequestDispatcher dispatcher = request.getRequestDispatcher("register.jsp");
				request.setAttribute("error", "Could not register");
				dispatcher.forward(request, response);
			}
		}
		else {
			RequestDispatcher dispatcher = request.getRequestDispatcher("register.jsp");
			request.setAttribute("error", "Could not register");
			request.setAttribute("types", LoanConstants.ID_TYPE);
			dispatcher.forward(request, response);
		}
		



		}

	}
