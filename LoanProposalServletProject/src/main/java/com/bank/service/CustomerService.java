package com.bank.service;

import com.bank.DAO.EmployeeDAO;
import com.bank.DAO.LoanDAO;
import java.util.ArrayList;
import com.bank.DAO.CustomerDAO;
import com.bank.constants.LoanConstants;
import com.bank.exception.EntityNotFoundException;
import com.bank.model.Collateral;
import com.bank.model.Customer;
import com.bank.model.Loan;

public class CustomerService {

	public Customer findCustomerByEmail(String email) throws EntityNotFoundException
	{
		Customer customer = CustomerDAO.findCustomerByEmail(email);
		if (customer == null)
			throw new EntityNotFoundException("Customer does not exist");
		return customer;
	}

	public boolean addCustomer(Customer customer)
	{
		if(CustomerDAO.insertCustomer(customer))
			return true;
		return false;
	}
	
}
