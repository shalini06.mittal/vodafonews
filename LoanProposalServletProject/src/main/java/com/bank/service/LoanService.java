package com.bank.service;

import java.util.ArrayList;

import com.bank.DAO.CustomerDAO;
import com.bank.DAO.EmployeeDAO;
import com.bank.DAO.LoanDAO;
import com.bank.constants.LoanConstants;
import com.bank.exception.EntityNotFoundException;
import com.bank.model.Collateral;
import com.bank.model.Customer;
import com.bank.model.Loan;

public class LoanService {
	public Loan findLoanById(String loanId) throws EntityNotFoundException
	{
		Loan loan = LoanDAO.findLoanById(loanId);
		if(loan==null) {
			throw new EntityNotFoundException("Loan Proposal not found");
		}
		return loan;
	}
	public static void main(String[] args) {
		findLoansByEmployeeId("EMP003");
	}
	public static ArrayList<Loan> findLoansByEmployeeId(String empid)
	{
		ArrayList<Loan> loans = null;
		loans = LoanDAO.findLoanByEmployeeId(empid);
		if(loans.size() > 0) {
			loans.stream().forEach(loan->{
				Customer customer = CustomerDAO.findCustomerByEmail(loan.getCustomer().getCustomerEmailId());
				loan.setCustomer(customer);
			});
		}
		for(Loan loan:loans)
			System.out.println(loan.getCustomer());
		return loans;
	}
	public boolean uploadCollateral(String loanId, Collateral collateral) 
	{
		return LoanDAO.insertLoanCollateral(loanId, collateral.getCollateralId());		

	}
	public Loan applyForLoan(String loanType, double loanAmount,
			double period, Customer customer)
	{
		Loan loan = new Loan();
		loan.setCustomer(customer);
		loan.setInterestRate(LoanConstants.calculateRate(period));
		loan.setPeriod(period);
		loan.setLoanType(loanType);
		loan.setLoanAmount(loanAmount);
		int index = (int)(Math.random()* (EmployeeDAO.getEmployees().size()));
		loan.setEmployee(EmployeeDAO.getEmployees().get(index));
		loan.setLoanId("ABKB00"+(LoanDAO.getLoanProposalCount()+1)+"_"+customer.getCustomerEmailId());
		LoanDAO.insertLoanProposal(loan);
		return loan;
	}

}
