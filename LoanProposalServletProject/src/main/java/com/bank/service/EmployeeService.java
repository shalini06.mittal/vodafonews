package com.bank.service;

import java.util.ArrayList;

import com.bank.DAO.CustomerDAO;
import com.bank.DAO.EmployeeDAO;
import com.bank.DAO.LoanDAO;
import com.bank.exception.EntityNotFoundException;
import com.bank.model.Customer;
import com.bank.model.Loan;

public class EmployeeService {


	public void approveLoan(String eid) throws Exception {

		if(searchEmployeeById(eid)) {

			ArrayList<Loan> loans = LoanService.findLoansByEmployeeId(eid);
			if(loans.size()>0) {
				for(Loan loan:loans) {
					Customer customer = CustomerDAO.findCustomerByEmail(loan.getCustomer().getCustomerEmailId());
					if(loan.getLoanAmount() > (10 * customer.getAnnualIncome())) {
						loan.setRemarks("Loan amount cannot be 10 times of annual income");
						LoanDAO.updateLoanStatus(false, "Loan amount cannot be 10 times of annual income", loan.getLoanId());
					}
					else if(loan.getCollaterals() == null) {
						loan.setRemarks( "No collateral submitted");
						LoanDAO.updateLoanStatus(false, "No collateral submitted", loan.getLoanId());
					}
					else if(!customer.isIncomeTaxReturnAttached()) {
						loan.setRemarks( "Income proof not attached");
						LoanDAO.updateLoanStatus(false, "Income proof not attached", loan.getLoanId());
					}
					else if(customer.getCustomerIdentity() == null) {
						loan.setRemarks( "Identity document not submitted");
						LoanDAO.updateLoanStatus(false, "Identity document not submitted", loan.getLoanId());
					}
					else
					{
						loan.setRemarks( "Approved");
						loan.setApproved(true);
						LoanDAO.updateLoanStatus(true, "Approved", loan.getLoanId());
					}
				}
			}
			else {
				throw new Exception("No loan proposals against this employee id");
			}
		}
		else
			throw new EntityNotFoundException("Employee does not exist");
	}
	public boolean searchEmployeeById(String empid)
	{
		return EmployeeDAO.getEmployees().stream()
				.filter(employee -> empid.equals(employee.getEmployeeId()))
				.findAny()
				.orElse(null) !=null;

	}
}
