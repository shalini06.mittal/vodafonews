package com.bank.test;

import java.util.ArrayList;
import java.util.Scanner;

import com.bank.DAO.LoanDAO;
import com.bank.constants.LoanConstants;
import com.bank.exception.EntityNotFoundException;
import com.bank.model.Collateral;
import com.bank.model.Customer;
import com.bank.model.Loan;
import com.bank.service.CustomerService;
import com.bank.service.EmployeeService;
import com.bank.service.LoanService;

public class UserInterface {

	public static void main(String[] args) {

		//DatabaseStructure.createTables();
		//EmployeeDAO.fetchCollaterals().forEach(System.out::println);
		System.out.println("Choose from the following:-");
		System.out.println("1. Employee Login to process loans");
		System.out.println("2. New Customer");
		System.out.println("3. Existing Customer");
		Scanner sc = new Scanner(System.in);
		int choice = sc.nextInt();
		switch(choice)
		{
		case 1:
			System.out.println("Please enter your employee id");
			String empid = sc.next();
			EmployeeService service = new EmployeeService();

			try {
				service.approveLoan(empid);
			} catch (EntityNotFoundException e) {
				// TODO Auto-generated catch block
				System.out.println(e.getMessage());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				System.out.println(e.getMessage());
			}
			break;
		case 2:registerCustomer(sc);
		break;
		case 3:
			loginCustomer(sc);
			break;
		default:

		}
	}
	public static void loginCustomer(Scanner sc)
	{
		System.out.println("Please enter your registered email id");
		String email = sc.next();
		CustomerService service = new CustomerService();
		LoanService loanService = new LoanService();
		try{

			Customer customer = service.findCustomerByEmail(email) ;
			System.out.println("Welcome "+customer.getCustomerName());
			System.out.println("What would you like to do?");
			System.out.println("1. Upload collaterals");
			System.out.println("2. View Loan Proposals");
			System.out.println("3. View Profile");
			System.out.println("4. Apply for Loan");
			int option = sc.nextInt();
			switch(option) {
			case 1:
				char ans ='n';
				System.out.println("enter loanid");
				String loanId = sc.next();
				try {
					Loan loan = loanService.findLoanById(loanId);
					do {
						System.out.println("Choose collateral ID to upload");
						LoanConstants.COLLATERAL_TYPE.forEach(type->{
							System.out.println(type.getCollateralId()+" -> "+type.getCollateralType());
						});
						String collateralid = sc.next();
						loanService.uploadCollateral(loanId, LoanConstants.COLLATERAL_TYPE.stream().filter(collateral-> collateral.getCollateralId().equals(collateralid)).findAny().get());
						System.out.println("Would you upload more collaterals? Enter y or Y to upload or press any key to exit");
						ans = sc.next().charAt(0);
					}while(ans=='Y' || ans =='y');
				}catch (EntityNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					System.out.println(e.getMessage());
				}
				break;
			case 2: ArrayList<Loan> loans = LoanDAO.findLoanByCustomerId(email);
			if(loans.size() > 0)
				loans.stream().forEach(loan -> {
					System.out.println("Id "+loan.getLoanId());
					System.out.println("Type "+loan.getLoanType());
					System.out.println("Loan Amount "+loan.getLoanAmount());
					System.out.println("Period "+loan.getPeriod());
					System.out.println("Interest Rate "+loan.getInterestRate());
					System.out.println("Employee ID Assigned "+loan.getEmployee().getEmployeeId());
					System.out.println("Loan Type "+loan.getLoanType());
					loan.getCollaterals().forEach(System.out::println);
				});
			else
				System.out.println("No loan proposals found");
			break;
			case 3:
				try {
					Customer cust = service.findCustomerByEmail(email);
					System.out.println("Name "+cust.getCustomerEmailId());
					System.out.println("Address "+cust.getCustomerAddress());
					System.out.println("Annual Income "+cust.getAnnualIncome());
					System.out.println("Identity submitted "+cust.getCustomerIdentity());
					System.out.println();
				}catch (EntityNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					System.out.println(e.getMessage());
				}
				break;
			case 4:
				System.out.println("Choose loantype from the following: Select number");
				int x = 1;
				for(String loantype :LoanConstants.LOAN_TYPE) {
					System.out.println(x++ +". "+loantype);
				}
				String loantype = null;
				int ltype = sc.nextInt();
				switch(ltype) {
				case 1:loantype = LoanConstants.LOAN_TYPE[0];
				break;
				case 2:loantype = LoanConstants.LOAN_TYPE[1];
				break;
				case 3:loantype = LoanConstants.LOAN_TYPE[2];
				break;
				case 4:loantype = LoanConstants.LOAN_TYPE[3];
				break;
				case 5:loantype = LoanConstants.LOAN_TYPE[4];
				break;
				}
				System.out.println("Enter loan amount");
				double loanamount = sc.nextDouble();
				System.out.println("enter loan period");
				double period = sc.nextDouble();
				Loan loan = loanService.applyForLoan(loantype, loanamount, period, customer);
				System.out.println("Here is your Loan ID, please save for future reference");
				System.out.println("Loan Id : "+loan.getLoanId());
				break;
			default:
			}
		}catch(EntityNotFoundException e) {
			System.out.println("Not a registered customer");
		}
	}
	public static void registerCustomer(Scanner sc)
	{
		System.out.println("Please provide your details");
		System.out.println("Enter your email id");
		String email = sc.next();
		System.out.println("Enter you name");
		sc.nextLine();
		String name = sc.nextLine();
		System.out.println("Enter your address");
		String address = sc.nextLine();
		System.out.println("Choose any one of the below identity proof provided");
		for(int i=0;i<LoanConstants.ID_TYPE.length;i++)
			System.out.println(i+1 +". "+LoanConstants.ID_TYPE[i]);
		int type = sc.nextInt();
		String identity = null;
		switch (type) {
		case 1:
			identity = LoanConstants.ID_TYPE[0];
			break;
		case 2:
			identity = LoanConstants.ID_TYPE[1];
			break;
		case 3:
			identity = LoanConstants.ID_TYPE[2];
			break;
		default:System.out.println("You have not selected any identity proof");
		break;
		}
		System.out.println("Enter your annual income");
		double income = sc.nextDouble();
		System.out.println("Enter Y/y if income tax return attached or Enter N/n if not attached");
		char taxreturn = sc.next().charAt(0);
		Customer customer = new Customer();
		customer.setCustomerEmailId(email);
		customer.setCustomerName(name);
		customer.setCustomerAddress(address);
		customer.setCustomerIdentity(identity);
		customer.setAnnualIncome(income);
		customer.setIncomeTaxReturnAttached(taxreturn=='y' || taxreturn == 'Y' ?true:false);
		CustomerService service = new CustomerService();
		if(service.addCustomer(customer)) {
			System.out.println("Registered Successfully-------");
			System.out.println();
			System.out.println("Please login to continue");
			loginCustomer(sc);
		}
		else
			System.out.println("Could not register please contact admin");
	}

}
