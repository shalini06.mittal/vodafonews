package com.vodafone.HibernateMavenDemo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity // used to map the java class with the table name. Default is the classname
//@Table(name="emp")// if database table name is different than the class name use @Table annotation
public class Employee {

	@Id // for mapping with the primary key
	@Column(name="empid")
	private int eid;
	@Column(name="empname", length = 50, nullable = false)
	private String ename;
	private String city;
	private String email;
	@Column(name="sal")
	private double salary;
	
	public Employee() {
		// TODO Auto-generated constructor stub
	}

	public Employee(int eid, String ename, String city, String email, double salary) {
		super();
		this.eid = eid;
		this.ename = ename;
		this.city = city;
		this.email = email;
		this.salary = salary;
	}

	public int getEid() {
		return eid;
	}

	public void setEid(int eid) {
		this.eid = eid;
	}

	public String getEname() {
		return ename;
	}

	public void setEname(String ename) {
		this.ename = ename;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	@Override
	public String toString() {
		return "Employee [eid=" + eid + ", ename=" + ename + ", city=" + city + ", email=" + email + ", salary="
				+ salary + "]";
	}
	
	
}
