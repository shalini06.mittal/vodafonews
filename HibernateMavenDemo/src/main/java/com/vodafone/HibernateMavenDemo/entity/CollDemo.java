package com.vodafone.HibernateMavenDemo.entity;

import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="coll")
public class CollDemo {

	@Id
	private int id;
	
	private String str;
	
	@ElementCollection//(fetch = FetchType.EAGER)
	@JoinTable(name="list_data", joinColumns = @JoinColumn(name="id") )
	@Column(name="data")
	private List<String> list;
	
//	@ElementCollection
//	private Set<String> set;
	
	@OneToMany
	private List<Employee> emails;
	
	@ElementCollection
	private Map<String, String> map;
	
	
	public String getStr() {
		return str;
	}
	public void setStr(String str) {
		this.str = str;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public List<String> getList() {
		return list;
	}
	public void setList(List<String> list) {
		this.list = list;
	}
//	public Set<String> getSet() {
//		return set;
//	}
//	public void setSet(Set<String> set) {
//		this.set = set;
//	}
	public List<Employee> getEmails() {
		return emails;
	}
	public void setEmails(List<Employee> emails) {
		this.emails = emails;
	}
	public Map<String, String> getMap() {
		return map;
	}
	public void setMap(Map<String, String> map) {
		this.map = map;
	}
	
	
}
