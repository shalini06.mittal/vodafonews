package com.vodafone.HibernateMavenDemo.entity;

import javax.persistence.CascadeType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
public class Supplier {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int sid;
	private String sname;
	//@Embedded
	// uni-directional mapping
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="email")
	private EmailEntity emailEntity;
	
	public int getSid() {
		return sid;
	}
	public void setSid(int sid) {
		this.sid = sid;
	}
	public String getSname() {
		return sname;
	}
	public void setSname(String sname) {
		this.sname = sname;
	}
	public EmailEntity getEmailEntity() {
		return emailEntity;
	}
	public void setEmailEntity(EmailEntity emailEntity) {
		this.emailEntity = emailEntity;
	}
	@Override
	public String toString() {
		return "Supplier [sid=" + sid + ", sname=" + sname + ", emailEntity=" + emailEntity + "]";
	}
	
	
	

}
