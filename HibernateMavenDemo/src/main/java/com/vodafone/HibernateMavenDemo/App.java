package com.vodafone.HibernateMavenDemo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.vodafone.HibernateMavenDemo.entity.EmailEntity;
import com.vodafone.HibernateMavenDemo.entity.Supplier;

/**
 * 1) add hibernate jar files [maven or non-maven]
 * 2) Craete an entity class
 * 3) Add annotations to the entity class
 * 4) create hibernate.cfg.xml file to configure database related parameters
 * 
 * ways to congifure mapping of classes withh tables
 * 1) using mapping xml file
 * 2) annotation based and configure in hibernate.cfg.xml
 * 3) annotation based but do not configure in hibernate.cfg.xml . Add it programmatically
 * 
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        EmailEntity e1 = new EmailEntity();
        e1.setCity("Delhi");
        e1.setEmail("a@s.c");
        
        Supplier s1 = new Supplier();
        s1.setEmailEntity(e1);
        s1.setSname("Udemy");
        
        SessionFactory f = HIbernateUtility.getSessionFactory();
//        Session session = f.openSession();
////        Transaction tx = session.beginTransaction();
////        //session.save(e1);
////        session.save(s1);
////        tx.commit();
//        EmailEntity p = session.get(EmailEntity.class, 1);
//        System.out.println(p.getCity()+" "+p.getEmail());
//        System.out.println(p.getSupplier().getSname());
//        session.close();
    }
}
