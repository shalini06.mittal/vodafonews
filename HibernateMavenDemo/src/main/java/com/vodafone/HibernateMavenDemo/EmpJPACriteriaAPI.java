package com.vodafone.HibernateMavenDemo;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import com.vodafone.HibernateMavenDemo.entity.Employee;
/*
 * 
 * Get a CriteriaBuilder before working on your query.
 * Use CriteriaBuilder instead of Hibernate’s Session interface to create your CriteriaQuery.
 * Call the from method on your CriteriaQuery to start defining your FROM clause.
 * Use the join method of the Root interface to define JOIN clauses instead of createCriteria or createAlias.
 * Create Expressions using the CriteriaBuilder to define your WHERE clause instead of calling the add method on the Criteria interface. 
 * The resulting code is much more verbose and often harder to read than your previous Hibernate-specific Criteria query.
 * Call groupBy, having and orderBy on the CriteriaQuery interface to define your GROUP BY, HAVING, and ORDER BY clauses. This approach is very similar to the generated SQL statement. Using Hibernate’s deprecated Criteria query, you defined these clauses as part of your projection.
 */
public class EmpJPACriteriaAPI {

	private static SessionFactory factory = HIbernateUtility.getSessionFactory();
	
	public static void main(String[] args) {
		Session session = factory.openSession();
		
		CriteriaBuilder builder = session.getCriteriaBuilder();
		// define the query to be operated on which class
		CriteriaQuery<Employee> query = builder.createQuery(Employee.class);
		// defines the from clause
		Root<Employee> root = query.from(Employee.class);
		// select query
		query.select(root);
		
		Query<Employee> q1 = session.createQuery(query);
		for(Employee emp: q1.getResultList())
			System.out.println(emp);
		
		session.close();
	}

}
