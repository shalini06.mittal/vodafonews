package com.vodafone.HibernateMavenDemo;


import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import com.vodafone.HibernateMavenDemo.entity.Employee;

public class EmployeeCrud {

	private static SessionFactory factory = HIbernateUtility.getSessionFactory();
	
	public static long count()
	{
		Session session = factory.openSession();
		Long c = (Long) session.createQuery("select count(*) from Employee").getSingleResult();
		return c;
	}
	public static void addEmployee(Employee emp)
	{
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();
		Integer id = (Integer) session.save(emp);
		System.out.println(id);
		tx.commit();
		session.close();
	}
	public static void deleteEmployee(Employee emp)
	{
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();
		session.delete(emp);
		tx.commit();
		session.close();
	}
	public static void updateEmployee(Employee emp)
	{
		Session session = factory.openSession();
		Transaction tx = session.beginTransaction();
		session.update(emp);
		tx.commit();
		session.close();
	}
	
	public static List<Employee> getEmployees()
	{
		Session session = factory.openSession();
		List<Employee> emps = session.createQuery("from Employee", Employee.class).list();
		session.close();
		return emps;
	}
	// HQL
	public static List<Employee> getEmployeesByCity(String city)
	{
		Session session = factory.openSession();
		Query<Employee> query = session.createQuery("from Employee where city=:city", Employee.class);
		query.setParameter("city", city);
		List<Employee> emps = query.list();
		session.close();
		return emps;
	}
	public static List<Employee> getEmployeesByCity(Employee emp)
	{
		Session session = factory.openSession();
		Query<Employee> query = session.createQuery("from Employee where city=:city and email =:email", Employee.class);

		query.setParameter("city", emp.getCity());
		query.setParameter("email", emp.getEmail());
		
		List<Employee> emps = query.list();
		session.close();
		return emps;
	}
	public static Employee getEmployeeById(int id)
	{
		Session session = factory.openSession();
		Employee emp = session.get(Employee.class, id);
		session.close();
		return emp;
	}
	public static Employee loadEmployeeById(int id)
	{
		Session session = factory.openSession();
		// lazy fetch
		Employee emp = session.load(Employee.class, id);
		System.out.println(emp.getEid());
		System.out.println(emp.getCity());
		session.close();
		return emp;
	}
	public static void main(String[] args) {
		Employee e1 = new Employee(5, "Tim", "Mumbai", "tim@gmail.com", 10000);
		Employee e2 = new Employee(6, "Rui", "Pune", "rui@gmail.com", 12000);
		Employee e3 = new Employee(7, "Marcel", "Mumbai", "marcel@gmail.com", 20000);
		Employee e4 = new Employee(8, "Pedro", "Chennai", "pedro@gmail.com", 15000);
		
		addEmployee(e1);
		addEmployee(e2);
		addEmployee(e3);
		addEmployee(e4);
		
//		e1.setCity("Chennai");
//		updateEmployee(e1);
		
		//System.out.println(getEmployeeById(5));
		System.out.println(loadEmployeeById(1));
		
		
//		for(Employee emp:getEmployees())
//			System.out.println(emp);
//		System.out.println();
//		for(Employee emp:getEmployeesByCity("Mumbai"))
//			System.out.println(emp);
		System.out.println(count());

	}

}

