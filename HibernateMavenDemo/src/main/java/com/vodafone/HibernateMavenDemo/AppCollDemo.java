package com.vodafone.HibernateMavenDemo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.vodafone.HibernateMavenDemo.entity.CollDemo;
import com.vodafone.HibernateMavenDemo.entity.EmailEntity;
import com.vodafone.HibernateMavenDemo.entity.Employee;
import com.vodafone.HibernateMavenDemo.entity.Supplier;

/**
 * 1) add hibernate jar files [maven or non-maven]
 * 2) Craete an entity class
 * 3) Add annotations to the entity class
 * 4) create hibernate.cfg.xml file to configure database related parameters
 * 
 */
public class AppCollDemo 
{
    public static void main( String[] args )
    {
        List<String> l1 = new ArrayList<String>();
        l1.add("s1");
        l1.add("s2");
        
        Map<String, String> map = new HashMap<String, String>();
        map.put("k1", "v1");
        map.put("k2", "v2");
        
        Employee e1 = new Employee(5, "Tim", "Mumbai", "tim@gmail.com", 10000);
		Employee e2 = new Employee(6, "Rui", "Pune", "rui@gmail.com", 12000);
		
        List<Employee> emp = new ArrayList<>();
        emp.add(e1);
        emp.add(e2);
        
        CollDemo d1 = new CollDemo();
        d1.setEmails(emp);
        d1.setId(1);
        d1.setList(l1);
        d1.setMap(map);
        d1.setStr("Something");
        
        SessionFactory f = HIbernateUtility.getSessionFactory();
        Session session = f.openSession();
//        Transaction tx = session.beginTransaction();
//        session.save(e1);
//        session.save(e2);
//        session.save(d1);
//        tx.commit();
        CollDemo d2 = session.get(CollDemo.class, 1);
        System.out.println(d2.getList());
        session.close();
        System.out.println(d2.getStr());
        System.out.println(d2.getList());
        
    }
}
