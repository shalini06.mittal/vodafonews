package com.vodafone.HibernateMavenDemo;

import java.util.List;


import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import com.vodafone.HibernateMavenDemo.entity.Employee;
/*
 * 
 * Get a CriteriaBuilder before working on your query.
 * Use CriteriaBuilder instead of Hibernate’s Session interface to create your CriteriaQuery.
 * Call the from method on your CriteriaQuery to start defining your FROM clause.
 * Use the join method of the Root interface to define JOIN clauses instead of createCriteria or createAlias.
 * Create Expressions using the CriteriaBuilder to define your WHERE clause instead of calling the add method on the Criteria interface. 
 * The resulting code is much more verbose and often harder to read than your previous Hibernate-specific Criteria query.
 * Call groupBy, having and orderBy on the CriteriaQuery interface to define your GROUP BY, HAVING, and ORDER BY clauses. This approach is very similar to the generated SQL statement. Using Hibernate’s deprecated Criteria query, you defined these clauses as part of your projection.
 */
public class EmpHibernateCriteriaAPI {

	private static SessionFactory factory = HIbernateUtility.getSessionFactory();
	
	public static void main(String[] args) {
		Session session = factory.openSession();
		
		Criteria criteria = session.createCriteria(Employee.class);
		
		//criteria.add((Restrictions.between("salary", 10000.0, 250000.0)));
		criteria.setMaxResults(5);
		List<Employee> list = criteria.list();
		for(Employee emp: list)
			System.out.println(emp);
		
		session.close();
	}

}
