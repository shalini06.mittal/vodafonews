package com.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import com.model.Customer;

public class DeleteCustomer {
	
	public static boolean deleteCustomer(int cid)
	{
//		String sql = "insert into customer values("+ 
//				customer.getCid()+",'"+customer.getCname()+"','"+customer.getCity()+"','"+
//				customer.getPhone()+"','"+customer.getEmail()+
//				"')";
		
		String sql = "delete from customer where cid=?";
		Connection con = MySQLComnnection.getConnection();
		try {
			//Statement statement = con.createStatement();
			PreparedStatement statement = con.prepareStatement(sql);
			statement.setInt(1, cid);
			
			statement.execute();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		return true;	
	}
}
