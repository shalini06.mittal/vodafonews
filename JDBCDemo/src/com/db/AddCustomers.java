package com.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import com.model.Customer;

public class AddCustomers {
	
	public static boolean addCustomer(Customer customer)
	{
//		String sql = "insert into customer values("+ 
//				customer.getCid()+",'"+customer.getCname()+"','"+customer.getCity()+"','"+
//				customer.getPhone()+"','"+customer.getEmail()+
//				"')";
		
		String sql = "insert into customer values(?,?,?,?,?)";
		Connection con = MySQLComnnection.getConnection();
		try {
			//Statement statement = con.createStatement();
			PreparedStatement statement = con.prepareStatement(sql);
			statement.setInt(1, customer.getCid());
			statement.setString(2, customer.getCname());
			statement.setString(3, customer.getCity());
			statement.setString(4, customer.getPhone());
			statement.setString(5, customer.getEmail());
			statement.execute();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		return true;	
	}
}
