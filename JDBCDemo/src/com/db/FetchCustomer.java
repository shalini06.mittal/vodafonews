package com.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.model.Customer;

public class FetchCustomer {

	
	public static List<Customer> getAllCustomers()
	{
		List<Customer> customers = new ArrayList<Customer>();
		Connection con = MySQLComnnection.getConnection();
		String sql = "select * from customer";
		try {
			PreparedStatement st = con.prepareStatement(sql);
			ResultSet rs = st.executeQuery();
			while(rs.next())
			{
				Customer customer = new Customer();
				customer.setCid(rs.getInt(1));
				customer.setCname(rs.getString(2));
				customer.setCity(rs.getString(3));
				customer.setPhone(rs.getString(4));
				customer.setEmail(rs.getString(5));
				customers.add(customer);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			try {
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return customers;
		
	}
	public static List<Customer> getCustomerByCity(String city)
	{
		List<Customer> customers = new ArrayList<Customer>();
		Connection con = MySQLComnnection.getConnection();
		String sql = "select cname, city, email from customer"
				+" where city=?";
		try {
			PreparedStatement st = con.prepareStatement(sql);
			st.setString(1, city);
			ResultSet rs = st.executeQuery();
			while(rs.next())
			{
				Customer customer = new Customer();
			
				customer.setCname(rs.getString(1));
				customer.setCity(rs.getString(2));
			
				customer.setEmail(rs.getString(3));
				customers.add(customer);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			try {
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return customers;
		
	}
}
