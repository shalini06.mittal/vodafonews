package utility;

public class Constants {

	public static final String DBMANAGER = "DBManager";
	
	public static final String NAME = "cname";
	
	public static final String PASSWORD = "password";
	public static final String EMAIL = "email";
	public static final String GENDER = "gender";
	public static final String CITY = "city";
	public static final String PHONE = "phone";
	public static final String FEMALE = "female";
	public static final String MALE = "male";
}
