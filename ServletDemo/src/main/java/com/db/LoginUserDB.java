package com.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.model.LoginUser;

public class LoginUserDB {
	
	
	public String validateUser(LoginUser user)
	{
		Connection con = MySQLComnnection.getConnection();
		
		String sql = "select password, role from loginuser where email = ?";
		try {
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, user.getEmail());
			ResultSet rs = ps.executeQuery();
			if(rs.next())
			{
				if(rs.getString(1).equals(user.getPassword()))
					return rs.getString(2);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		
		return null;
	}

}
