package com.model;

public class Customer {
	
	private int cid;
	private String cname;
	private String city;
	private String phone;
	private String email;
	
	public Customer() {
		// TODO Auto-generated constructor stub
	}

	public Customer(int cid, String cname, String city, String phone, String email) {
		super();
		this.cid = cid;
		this.cname = cname;
		this.city = city;
		this.phone = phone;
		this.email = email;
	}

	
	public int getCid() {
		return cid;
	}

	public void setCid(int cid) {
		this.cid = cid;
	}

	public String getCname() {
		return cname;
	}

	public void setCname(String cname) {
		this.cname = cname;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "Customer [cid=" + cid + ", cname=" + cname + ", city=" + city + ", phone=" + phone + ", email=" + email
				+ "]";
	}

}
