package com.filter;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * CacheFilter => /profile and /welcome and put the repetetive code of 
 * caching in this filter
 */
/**
 * Servlet Filter implementation class LogFilter
 */
//@WebFilter(urlPatterns = {"/login","/welcome"})
public class LogFilter extends HttpFilter implements Filter {     
    /**
     * @see HttpFilter#HttpFilter()
     */
    public LogFilter() {
       System.out.println("Log filter constructor");
    }
	public void destroy() {
		System.out.println("Log filter destroy");
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		System.out.println("Log filter dofiler before request to servlet");
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse resp = (HttpServletResponse) response;
		String email = req.getParameter("email");
		if(email != null)
			if(email.endsWith("vf.com"))
				chain.doFilter(request, response);
			else
				resp.sendRedirect("index.html");
		else
			chain.doFilter(request, response);
		System.out.println("Log filter dofiler after response from servlet");
		//resp.setContentType("text/html");
//		PrintWriter out = resp.getWriter();
//		out.println("<h1>Response from filter</h1>");
		resp.setStatus(201);
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		System.out.println("Log filter init");
	}

}
