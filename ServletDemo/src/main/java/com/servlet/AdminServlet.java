package com.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.db.CustomerDB;
import com.model.Customer;

/**
 * Servlet implementation class ProfileServlet
 */
@WebServlet("/admin")
public class AdminServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdminServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		CustomerDB db = new CustomerDB();
		List<Customer> customers = db.getAllCustomers();
		
		
		PrintWriter out = response.getWriter();	
		response.setContentType("text/html");
		out.println("<html><head><style>table {border-spacing: 15px;border-bottom: "
				+ "1px scrollbar solid;border-collapse: collapse;}"
				+"th, td {border: 1px solid black;text-align: center;padding: 35px;}"
				+ "body{margin:30px;background-color: bisque;}</style></head><body>");
		out.write("<h1>Welcome Admin</h1>");
		out.println("<table><tr><th>Name</th><th>City</th><th>Phone</th><th>Email</th><th>Action</th>");
		for(Customer customer : customers)
		{
			out.println("<tr><td>"+customer.getCname()+"</td>");
			out.println("<td>"+customer.getCity()+"</td>");
			out.println("<td>"+customer.getPhone()+"</td>");
			out.println("<td>"+customer.getEmail()+"</td>");
			out.println("<td><a href='edit?id="+customer.getCid()+"'>Edit</a> &nbsp; <a href='delete?id="+customer.getCid()+"'>Delete</a>");
			out.println("</tr>");
		}
		out.println("</table>");
		out.println("<div><a href='logout'>Logout</a>");
		out.println("</body></html");
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
