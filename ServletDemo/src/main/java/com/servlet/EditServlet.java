package com.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.db.CustomerDB;
import com.model.Customer;
import static utility.Constants.*;

/**
 * Servlet implementation class DeleteServlet
 */
@WebServlet("/edit")
public class EditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EditServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    /*
     * 1) From AdminServlet pass the customer id to editservlet doGet()
     * 2) Create getCustomerById(int cid) and returns the customer object in CustomerDB.java
     * 3) Create  updateCustomer(Customer customer) and returns true or false in CustomerDB.java
     * 4) In EditServlet doGet() access the id passed and display an html form within this doGet()
     * with action='edit' method='post. Fill in the form with the customer data that you retrieve
     * from the database with the id
     * 5) when the user clicks on submit button the doPost() of this EditServlet will be called
     * where you will call the updateCustomer(Customer customer) and if updated successfully redirect
     * it to admin servlet
     * 
     */

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		int id = Integer.parseInt(request.getParameter("id"));
		CustomerDB database = new CustomerDB();
		Customer customer = database.getCustomerById(id);
		out.println("<html><head><link rel=\"stylesheet\"\n"
				+ "	href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\"><head><body><h1 class='text-center'>Edit!!</h1>");
		out.println("<form action='edit' method='post'>");
		out.println("<div class=\"row\"><div class=\"col-lg-6 col-lg-offset-3\">");
		
		out.println("<div class=\"form-group\">");
		out.println("<div>Email : <input class=\"form-control\" type='text' value='"+customer.getEmail()+"' name='email' readonly='readonly'></div></div>");
		
		out.println("<div class=\"form-group\">");
		out.println("<div>Name : <input type='text' class=\"form-control\" value='"+customer.getCname()+"' name='cname'></div></div>");
		
		out.println("<div class=\"form-group\">");
		out.println("<div>Phone : <input type='text' class=\"form-control\" value='"+customer.getPhone()+"' name='phone'></div></div>");
		
		out.println("<div class=\"form-group\">");
		out.println("<div>City : <input type='text' class=\"form-control\" value='"+customer.getCity()+"' name='city'></div></div>");
		
		out.println("<div class=\"form-group\">");
		out.println("<input type='hidden' value='"+customer.getCid()+"' name='id'></div>");
		
		out.println("<div align=\"center\">");
		out.println("<input type='submit' class=\"btn btn-primary\" value='Edit'></button>");
		out.println("<button class=\"btn btn-primary\" value='Cancel' (click)='location.href(\"admin\")'>Cancel</button></div>");
		out.println("</form></body></html");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("do post");
		String name=request.getParameter(NAME);
		int id = Integer.parseInt(request.getParameter("id"));
		String phone=request.getParameter(PHONE);
		String city=request.getParameter(CITY);
		System.out.println(name);
		System.out.println(phone);
		System.out.println(city);
		

		response.setContentType("text/html");
		if( (name != null&&!name.isEmpty() )
				 && (phone != null && !phone.isEmpty() )
				
				&& ( city != null && !city.isEmpty()))
		{
			System.out.println("if not null");
			Customer c1 = new Customer();
			c1.setCid(id);
			c1.setCity(city);
			c1.setPhone(phone);
			c1.setCname(name);

			CustomerDB database = new CustomerDB();		
			if(database.updateCustomer(c1))
			{				
				response.sendRedirect("admin");
			}
		}
	}

}
