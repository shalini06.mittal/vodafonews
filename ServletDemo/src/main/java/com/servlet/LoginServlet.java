package com.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.sql.Connection;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.db.LoginUserDB;
import com.model.Customer;
import com.model.LoginUser;

/**
 * Servlet implementation class LoginServlet
 * 
 * 
 * 1) Add register.html and copy the code from codebunk and paste it
 * 2) Copy com.model.Customer.java from previous JDBCDemo project and paste in this project
 * 3) Create LoginUser.java under com.model package with email and password as properties. Create
 * getter/setter, constructor and toString
 * 4) Create RegisterServlet.java file and in the doPost method access all the customer information
 * passed from register.html file. 
 * 5) In LoginServlet, once you get email and password craete LoginUser object
 * 6) In RegisterServlet, once you get customer information create the Customer object
 * 
 * Fri 11 Feb
 * 1) Complete CustomerDb class for the following:
 * public  boolean addCustomer(Customer customer)
 * public  boolean deleteCustomer(int cid)
 * public List<Customer> getAllCustomers()
 * public Customer getCustomerByEmail(String email)
 * public  boolean updateCustomer(int cid)
 * 
 * 2) Complete ProfileServlet to display customer information
 * 3) Complete RegisterServlet to insert customer information in database and
 * for successful registration redirect to login page
 */
@WebServlet(urlPatterns = "/login", 
initParams = {@WebInitParam(name = "configFile", value = "config.xml"),
		@WebInitParam(name = "email", value = "shalini@gmail.com")}
		)
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public LoginServlet() {
		super();
		System.out.println("login servlet!!!");
		// TODO Auto-generated constructor stub
	}

	@Override
	public void init() throws ServletException {
		// TODO Auto-generated method stub
		super.init();
		System.out.println("init called "+getInitParameter("configFile"));
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		super.destroy();
		System.out.println("destroy called");
	}
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//		String message = request.getParameter("msg");
		//		System.out.println("do get");
		//		System.out.println(request.getContentType());
		//		System.out.println(request.getMethod());
		//		PrintWriter writer = response.getWriter();
		//		writer.println("<h1>"+message.toUpperCase()+"</h1>");
		response.sendRedirect("index.html");

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("do post");
		System.out.println(request.getMethod());
		String email = request.getParameter("email");
		String password = request.getParameter("password");	
		//		String role = request.getParameter("role");
		//		System.out.println("role "+role);

		LoginUser user = new LoginUser(email, password, null);
		LoginUserDB logindb = new LoginUserDB();
		PrintWriter writer = response.getWriter();	
		response.setContentType("text/html");

		
		Connection con = (Connection) request.getServletContext().getAttribute("con");
		System.out.println("connection object "+con);
		
		String role = logindb.validateUser(user);
		

		
		if(role != null)
		{
			// inter - servlet communiation
			writer.println("Valid Credentials");
			//			RequestDispatcher rd = request.getRequestDispatcher("welcome");
			//			rd.include(request, response);
			/*
			 * 1) URL rewriting
			 * 2) cookies
			 * 3) hidden form fields
			 * 4) HttpSession
			 */
			HttpSession session = request.getSession();
			session.setAttribute("email", email);
			response.addCookie(new Cookie("info", URLEncoder.encode("analytics info","UTF-8")));
			//response.sendRedirect("welcome?email="+email);
			if(role.equals("admin"))
			{
				response.sendRedirect("admin");
			}
			else
			response.sendRedirect("welcome");
			/**
			 * hidden form field
			 * <form action='welcome'>
			 * <input type='hidden' value=email/>
			 * </form>
			 */
		}
		else {

			writer.println("Invalid Credentials");
			RequestDispatcher rd = request.getRequestDispatcher("index.html");
			rd.include(request, response);
		}


	}

}
