package com.servlet;

import static utility.Constants.CITY;
import static utility.Constants.EMAIL;
import static utility.Constants.GENDER;
import static utility.Constants.NAME;
import static utility.Constants.PASSWORD;
import static utility.Constants.PHONE;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.db.CustomerDB;
import com.model.Customer;
import com.model.LoginUser;

/**
 * Servlet implementation class Register
 */
@WebServlet("/register")
public class RegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String name=request.getParameter(NAME);
		
		String password=request.getParameter(PASSWORD);
		String email=request.getParameter(EMAIL);
		String phone=request.getParameter(PHONE);
		
		String city=request.getParameter(CITY);
		
		
		PrintWriter writer = response.getWriter();	
		response.setContentType("text/html");
		if( (name != null && !name.trim().isEmpty())&& 
				(!!email.isEmpty() || email != null) && (!phone.isEmpty() && phone != null) && 
				 (!city.isEmpty() || city != null) )
		{
			System.out.println("if not null");
		
			Customer c1 = new Customer(0,name,city,phone, email);
			
			CustomerDB db = new CustomerDB();
			if(db.addCustomer(c1, password))
			{
				response.sendRedirect("index.html");
			}
			System.out.println(c1);
			
		}
		else{
			writer.println("Fill in all details");
			
			RequestDispatcher rd = request.getRequestDispatcher("register.html");
			rd.include(request, response);
			
		}
	}

}
