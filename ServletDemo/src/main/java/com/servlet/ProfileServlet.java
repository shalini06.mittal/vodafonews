package com.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.db.CustomerDB;
import com.model.Customer;

/**
 * Servlet implementation class ProfileServlet
 */
@WebServlet("/profile")
public class ProfileServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ProfileServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter writer = response.getWriter();
		//String email = request.getParameter("email");
//		response.setHeader("Cache-Control", "no-cache,no-store,must-revalidate");
//		response.setHeader("Pragma", "no-cache");//http1.0
//		response.setHeader("Pragma", "0");//proxies
		HttpSession session = request.getSession();
		String email = (String) session.getAttribute("email");
		if(email != null)
		{
			CustomerDB db = new CustomerDB();

			System.out.println(email);
			Customer customer = db.getCustomerByEmail(email);
			
			response.setContentType("text/html");
			writer.println("<html><head><style>h1 {color: green}</style></head><body>");
			writer.println("<h3>Welcome "+email+"</h3>");
			writer.println("<p> Name: "+customer.getCname()+"</p>");
			writer.println("<p> City: "+customer.getCity()+"</p>");
			writer.println("<p> Phone: "+customer.getPhone()+"</p>");
			writer.println("<div><a href=welcome?email="+email+">Welcome</a>");
			writer.println("<div><a href='logout'>Logout</a>");
			writer.println("</body></html");
		}
		else
			response.sendRedirect("index.html");

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
