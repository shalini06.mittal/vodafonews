package com.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class WelcomeServlet
 */
@WebServlet("/welcome")
public class WelcomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public WelcomeServlet() {
        super();System.out.println("weclome servlet"); // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter writer = response.getWriter();
		//String email = request.getParameter("email");
		// session object
//		response.setHeader("Cache-Control", "no-cache,no-store,must-revalidate");
//		response.setHeader("Pragma", "no-cache");//http1.0
//		response.setHeader("Pragma", "0");//proxies
		HttpSession session = request.getSession();
		String email = (String) session.getAttribute("email");
		if(email != null) {
		response.setContentType("text/html");
		writer.println("<html><head><style>h1 {color: green}</style></head><body>");
		writer.println("<h1>Welcome "+email+"</h1>");
//		writer.println("<div><a href=profile?email="+email+">Profile</a>");
		writer.println("<div><a href='profile'>Profile</a>");
		writer.println("<div><a href='logout'>Logout</a>");
		writer.println("</body></html");
		}
		else
			response.sendRedirect("index.html");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

}
