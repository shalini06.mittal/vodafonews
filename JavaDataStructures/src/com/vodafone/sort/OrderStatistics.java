package com.vodafone.sort;

import java.util.Arrays;

public class OrderStatistics {

	public static void swap(int a[], int i, int j)
	{
		int temp = a[i];
		a[i] = a[j];
		a[j] = temp;
	}
	public static int arrange(int a[], int s, int e) {

		int pivot = a[s];
		int i = s+1;
		int j = e;

		while(i<=j)
		{
			while(a[i] < pivot) i++;
			while(a[j] > pivot) j--;
			if(i<j)
			{
				swap(a, i,j);
			}
		}
		swap(a,j,s);
		return j;
	}

	public static int findKthsmallestElement(int a[],int l, int r, int k)
	{
		if(l<=r) {
			int pi = arrange(a, l, r);
			System.out.println(pi);
			if(pi == k-1) return a[k-1];
			else if(pi > k-1)
			{
				return findKthsmallestElement(a, l, pi-1, k);
			}
			else if(pi < k-1){
				return findKthsmallestElement(a, pi+1, r, k);
			}
		}
		return -1;
	}
	public static void main(String[] args) {

		int arr[] = {3,5,1,2,6,9,7};

		System.out.println(findKthsmallestElement(arr, 0, arr.length-1, 4));

	}

}
