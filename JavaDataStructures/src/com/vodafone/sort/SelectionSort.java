package com.vodafone.sort;

public class SelectionSort {

	public static void main(String[] args) {
		int arr[] = {10,14,13,5,6};
		
		for(int i=0;i<arr.length-1;i++) {
			int index = i;
			for(int l = index+1;l<arr.length;l++)
			{
				if(arr[l] < arr[index])
				{
					index = l;
				}
			}
			int temp = arr[index];
			arr[index] = arr[i];
			arr[i] = temp;

		}
		for(int val:arr)
			System.out.println(val);
	}

}
