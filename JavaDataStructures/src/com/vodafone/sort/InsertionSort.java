package com.vodafone.sort;

public class InsertionSort {

	public static void main(String[] args) {
		int arr[] = {10,4,13,5,60, 9, 1, 8};
		for(int j=1;j<arr.length;j++) {
			int key = arr[j];//4
			int i = j-1;
			while(i>=0 && arr[i] >= key)
			{
				arr[i+1] = arr[i];
				i--;
			}
			arr[i+1] = key;
		}
		for(int x:arr)
			System.out.println(x);
	}

}
