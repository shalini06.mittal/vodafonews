package com.vodafone.sort;

public class BInarySearch {

	public static void main(String[] args) {
		int a[] = {2,10,13,15,16,20,22};
		System.out.println(binarySearch(a, 20)); //5
		System.out.println(binarySearch(a, 21)); // -1
		System.out.println(binarySearch(a, 10)); //1
		System.out.println(binarySearch(a, 22)); //6

	}

	public static int binarySearch(int array[], int key)
	{
		int l = 0;
		int r = array.length-1;
		while(l<=r) {
			int mid = (l+r)/2;
			if(array[mid] == key)
				return mid;
			else if(array[mid] < key)
				l = mid+1;
			else
				r = mid-1;
		}
		return -1;
	}

}
