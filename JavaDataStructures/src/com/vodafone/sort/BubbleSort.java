package com.vodafone.sort;

import java.util.Scanner;

public class BubbleSort {

	/*
	 * take names of 5 fruits as input from the user and arrange them in the ascending order
	 * ex: banana grapes apples oranges chickoo apricot
	 * output : apples apricot banana chickoo grapes oranges
	 * compareTo
	 */
	
	public static void main(String[] args) {

		int arr[] = {10,4,13,5,60, 9, 1, 8};
		for(int j=0;j<arr.length-1;j++)
		{
			for(int i=0;i<arr.length-1-j;i++)
			{
				if(arr[i] > arr[i+1])
				{
					int temp = arr[i];
					arr[i] = arr[i+1];
					arr[i+1] = temp;
				}
			}
		}
		
		for(int val:arr)
			System.out.println(val);
		
		System.out.println();
		String arr1[] = {"one","two","three","four","five"};
		String[] fruitNames = new String[5];
		Scanner scanner = new Scanner(System.in);
        for (int i=0; i<5; i++) {
            
            System.out.println("Enter fruit number " + (i+1) + ":");
            fruitNames[i] = scanner.nextLine();
        }
        
        scanner.close();
        for (int j=0; j<fruitNames.length-1; j++) {
            for (int i=0; i<fruitNames.length-1; i++) {
                if (fruitNames[i].compareTo(fruitNames[i+1]) > 0) {
                    String temp = fruitNames[i];
                    fruitNames[i] = fruitNames[i+1];
                    fruitNames[i+1] = temp;
                }
            }
        }
        for(String fruit:fruitNames)
        	System.out.println(fruit);
	}

}
