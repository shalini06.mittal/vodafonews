package com.vodafone.ds;

public class StackDemo {

	/**
	 * FILO
	 * push
	 * pop
	 * peek 
	 */
	
	int stack[] = new int[5];
	int top;
	
	public StackDemo() {
		top = -1;
	}
	boolean isEmpty()
	{
		return top < 0;
	}
	boolean push(int ele)
	{
		if(top == stack.length-1)
		{
			System.out.println("stack is full");
			return false;
		}
		else {
			top++;
			stack[top] = ele;
			System.out.println(ele+" pushed in stack");
			return true;
		}
	}
	int pop()
	{
		if(isEmpty())
		{
			System.out.println("Stack Underflow");
			return -1;
		}
		return stack[top--];
	}
	public static void main(String[] args) {
		StackDemo d1 = new StackDemo();
		System.out.println(d1.pop());
		System.out.println(d1.push(10));
		System.out.println(d1.push(20));
		System.out.println(d1.push(30));
		System.out.println(d1.push(40));
		System.out.println(d1.pop());
		System.out.println(d1.push(50));
		System.out.println(d1.push(60));
		System.out.println(d1.push(70));
		System.out.println();
		System.out.println(d1.pop());
		System.out.println(d1.pop());
		System.out.println(d1.pop());
		System.out.println(d1.pop());
		System.out.println(d1.pop());
		System.out.println(d1.pop());

	}

}
