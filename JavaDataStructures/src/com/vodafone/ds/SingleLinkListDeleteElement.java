package com.vodafone.ds;

import java.awt.List;

public class SingleLinkListDeleteElement {

	Node head;
	static class Node
	{
		int data;
		Node next;
		Node(int data)
		{
			this.data = data;
			this.next = null;
		}
		
	}
	public static void insert(SingleLinkListDeleteElement list, int data)
	{
		//create the new node
		Node newNode = new Node(data);
		
		// check of list is empty then point head to newnode
		if(list.head== null)
		{
			list.head = newNode;
		}
		else {
			// iterate through the list to find the node whose next points to null
			Node last = list.head;
			while(last.next != null)
			{
				last = last.next;
			}
			last.next = newNode;
		}
		
	}
	public static void deleteByKey(SingleLinkListDeleteElement list, int key)
	{
		Node curr = list.head, prev = null;
		if(curr != null && curr.data == key)
		{
			list.head = curr.next;
			System.out.println(key + " found and deleted");
		}
		else {
			while(curr != null && curr.data != key)
			{
				prev = curr;
				curr= curr.next;
			}
			if(curr != null)
			{
				prev.next = curr.next;
				System.out.println(key + " found and deleted");
			}
			if(curr == null)
			{
				System.out.println(key + " not found");
			}
		}
		
	}
	public static void printList(SingleLinkListDeleteElement list)
	{
		Node current = list.head;
		while(current !=null)
		{
			System.out.print(current.data+" ");
			current = current.next;
		}
		System.out.println();
	}
	public static void main(String[] args) {
		SingleLinkListDeleteElement list = new SingleLinkListDeleteElement();
		insert(list, 1);
		insert(list, 10);
		insert(list, 5);
		insert(list, 3);
		insert(list, 8);
		insert(list, 2);
		insert(list, 13);
		printList(list);
		deleteByKey(list, 1);
		printList(list);
		deleteByKey(list, 3);
		printList(list);
		deleteByKey(list, 1);
		printList(list);
	}

}
