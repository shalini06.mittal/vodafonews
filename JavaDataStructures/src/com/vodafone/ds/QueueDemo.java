package com.vodafone.ds;

public class QueueDemo {

	/*
	 * FIFO
	 * capacity => max elements that a queue can hold
	 * size initially will be 0 and as you add elements it increases and queue is full when size == capacity 
	 */
	int front, rear, size;
	int capacity;
	int queue[];
	
	public QueueDemo(int capacity) {
		this.capacity = capacity;
		front = size = 0;
		rear = 0;
		queue = new int[capacity];
	}
	boolean isEmpty()
	{
		return this.size == 0;
	}
	// to add an element
	void enqueue(int ele)
	{
		// if queue is full cannot add
		if(size==capacity)
		{
			System.out.println("queue is full");
			return;
		}
		queue[rear++] = ele;
		size++;
		System.out.println(ele+" added");
	}
	// to remove an element
	int dequeue()
	{
		if(isEmpty())
			return -1;
		int data = queue[front];
		front++;
		size--;
		return data;
	}
	public static void main(String[] args) {
		
		QueueDemo d1 = new QueueDemo(5);
		System.out.println(d1.dequeue());
		d1.enqueue(5);
		d1.enqueue(4);
		d1.enqueue(3);
		d1.enqueue(2);
		d1.enqueue(1);
		d1.enqueue(15);
		System.out.println(d1.dequeue());
		System.out.println(d1.dequeue());
		System.out.println(d1.dequeue());
		System.out.println(d1.dequeue());
		// resolve the error?
		d1.enqueue(20);
		System.out.println(d1.dequeue());
		System.out.println(d1.dequeue());
	}

}
