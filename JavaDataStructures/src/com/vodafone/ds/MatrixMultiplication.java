package com.vodafone.ds;

public class MatrixMultiplication {

	public static void main(String[] args) {
		int m1[][] = {
				{1,2,3},
				{4,5,6}
		};
		int m2[][] = {
				{1,2},
				{3,4},
				{5,6}
		};
		// resultant matrix will be 1st matrix row count , 2nd matrix column count
		int product[][] = new int[m1.length][m2[0].length];
		for(int i=0;i<m1.length;i++)
		{
			for(int j=0;j<m2[0].length;j++)
			{
				for(int k=0;k<m2.length;k++)
				{
					product[i][j] = product[i][j] + m1[i][k] * m2[k][j];
				}
			}
		}
		for(int i=0;i<product.length;i++)
		{
			for(int j=0;j<product[i].length;j++)
			{
				System.out.print(product[i][j]+" ");
			}
			System.out.println();
		}
		
	}

}
