package com.vodafone.ds;

public class RangeQueries {

	static int k = 16;
    static int N = 100000; 
    static long table[][] = new long[N][k + 1]; 
    static void buildSparseTable(int arr[], int n) 
    { 
        for (int i = 0; i < n; i++) 
            table[i][0] = arr[i]; 
        for (int j = 1; j <= k; j++) {
        	System.out.println(1<<j);
        	int s = n - (1 << j);
        	System.out.println("s "+s);
            for (int i = 0; i <= s; i++) 
                table[i][j] = table[i][j - 1] + table[i + (1 << (j - 1))][j - 1];
        }
        for(int i=0;i<500;i++)
        {
        	for(int j=0;j<=k;j++)
        	{
        		System.out.print(table[i][j]+" ");
        	}
        	System.out.println();
        }
    } 
    static long query(int L, int R) 
    {
        long answer = 0; 
        for (int j = k; j >= 0; j--)  
        { 
            if (L + (1 << j) - 1 <= R)  
            { 
                answer = answer + table[L][j];
                L += 1 << j; 
            } 
        } 
        return answer; 
    }
    public static void main(String args[]) 
    { 
        int arr[] = { 3, 7, 2, 5, 8, 9 }; 
        int n = arr.length; 
        
        buildSparseTable(arr, n); 
        System.out.println(query(1, 4)); 
        System.out.println(query(4, 11)); 
        System.out.println(query(3, 5)); 
        System.out.println(query(2, 4)); 
        System.out.println(Math.log(6)/Math.log(2));
        System.out.println(Math.log(2)/Math.log(2));
        System.out.println(Math.log(3)/Math.log(2));
        System.out.println(Math.log(4)/Math.log(2));
        System.out.println(Math.log(8)/Math.log(2));
    } 


}
