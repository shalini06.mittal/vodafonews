package com.vodafone.ds;

public class CircularLinkList {

	
	Node head;
	
	static class Node{
		int data;
		Node next;
		Node(int data)
		{
			this.data = data;
			this.next = null;
		}
	}
	public void sortedInsert(int data)
	{
		Node newNode = new Node(data);
		Node curr = head;
		// check if head is null then head points to new node
		if(head==null)
		{
			newNode.next = newNode;
			head = newNode;
		}
		// if needs to inserted at the head
		else if(curr.data >= newNode.data)
		{
			while(curr.next != head)
			{
				curr = curr.next;
			}
			curr.next = newNode;
			newNode.next = head;
			head = newNode;
		}
		// if new node needs to be inserted in between or last
		else {
			while(curr.next != head && curr.next.data < newNode.data)
			{
				curr = curr.next;
			}
			newNode.next = curr.next;
			curr.next = newNode;
		}
		
	}
	void printList()
	{
		if(head != null)
		{
			Node curr = head;
			do
			{
				System.out.print(curr.data+" ");
				curr = curr.next;
			}while(curr != head);
		}
	}
	public static void main(String[] args) {
		System.out.println();
		CircularLinkList list = new CircularLinkList();
		list.sortedInsert(10);
		list.sortedInsert(5);
		list.sortedInsert(20);
		list.sortedInsert(15);
		list.sortedInsert(40);
		list.printList();

	}

}
