<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<h1>TagLib Demos</h1>
	<select>
		<c:forEach items="${fruits }" var="fruit">
			<option>${fruit }</option>
		</c:forEach>
	</select>

	<c:set var="msg" value="Hello there"></c:set>
	<p>JSP EXPRESSION LANGUAGE - EL</p>
	<h2>
		<c:out value="${msg }"></c:out>
	</h2>
	<c:set var="no" value="100"></c:set>
	<c:if test="${param.no == 100 }">
		<h1>Yes it is 100</h1>
	</c:if>

	<c:if test="${fn:contains(msg, 'Hello') }">
		<h2>Mesage is not null</h2>
	</c:if>
	<%-- https://www.informit.com/articles/article.aspx?p=30946&seqNum=4  --%>
	<c:if test="${msg eq 'Hello there' }">
		Message is equal
	</c:if>
	<c:if test="${msg ne 'Hello! there' }">
		Message is not  equal
	</c:if>
	<h1>For Each</h1>
	<c:forEach begin="1" end="10" var="i" varStatus="s" step='2'>
		<p>
			<c:out value="${i } : ${i*i } ${s.index } ${s.first }"></c:out>
		</p>
	</c:forEach>

	<c:forTokens items="Red, Green, Black, Blue" delims="," var="token">
		<h3>${token }</h3>

	</c:forTokens>


</body>
</html>