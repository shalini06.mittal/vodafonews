<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<h1>Welcome <%= session.getAttribute("email") %></h1>
<h2>Customer Profile</h2>
<p>Name : ${sessionScope.customer}</p>
<p>Name : ${sessionScope.customer.cname}</p>
<p>City : ${sessionScope.customer.city}</p>
<p>Phone : ${sessionScope.customer.phone}</p>
</body>
</html>