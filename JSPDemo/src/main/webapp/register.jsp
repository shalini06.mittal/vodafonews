<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Customer Registration System</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--  jQuery -->
<script type="text/javascript"
	src="https://code.jquery.com/jquery-1.11.3.min.js"></script>

<!-- Isolated Version of Bootstrap, not needed if your site already uses Bootstrap -->
<link rel="stylesheet"
	href="https://formden.com/static/cdn/bootstrap-iso.css" />

<!-- Bootstrap Date-Picker Plugin -->
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css" />
<style type="text/css">
.error {
	color: red;
	font-size: 20px;
}
</style>
</head>
<body>
	<h2 align="center">Customer Registration System</h2>
	<div class="bootstrap-iso">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-6 col-sm-6 col-xs-12">
					<form action="action.jsp" method="post">
						<div class="row">
							<div class="col-lg-6 col-lg-offset-3">

								<div class="form-group">
									<label for="name">Name:</label> <input type="text"
										class="form-control" id="name" placeholder="Enter Name"
										name="cname">
								</div>

								<div class="form-group">
									<label for="phone">Phone:</label> <input type="number"
										class="form-control" id="phone" placeholder="Enter phone no."
										name="phone">
								</div>
								<div class="form-group">
									<label for="email">Email:</label> <input type="email"
										class="form-control" id="email" placeholder="Enter email"
										name="email">
								</div>
								<div class="form-group">
									<label for="username">City:</label> <input type="text"
										class="form-control" id="city" placeholder="Enter City"
										name="city">
								</div>
								<div class="form-group">
									<label for="username">Password:</label> <input type="password"
										class="form-control" id="city" placeholder="Enter password"
										name="password">
								</div>
								<div class="form-group">
									<!-- Date input -->
									<label class="control-label" for="date">Date</label> <input
										class="form-control" id="date" name="date"
										placeholder="MM/DD/YYY" type="text" />
								</div>

								<div align="center">
									<input type="submit" class="btn btn-primary" value="Register" />
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
					$(document)
							.ready(
									function() {
										var date_input = $('input[name="date"]'); //our date input has the name "date"
										var container = $('.bootstrap-iso form').length > 0 ? $(
												'.bootstrap-iso form').parent()
												: "body";
										var options = {
											format : 'mm/dd/yyyy',
											container : container,
											todayHighlight : true,
											autoclose : true,
										};
										date_input.datepicker(options);
									})
				</script>
</body>
</html>