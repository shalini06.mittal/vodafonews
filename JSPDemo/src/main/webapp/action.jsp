<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<h2>Action Tags In JSP</h2>

<!-- 
	static
 -->

 
 <!-- 
	dynamic
 -->
 	<%
 		out.println("<h1>Date "+request.getParameter("date")+"</h1>");
 	%>
	<jsp:include page="header.html"></jsp:include>
	<%--
		Customer customer = enw Customer();
	 --%>
	<jsp:useBean id="customer" class="com.model.Customer">

	</jsp:useBean>
		<jsp:setProperty property="*" name="customer"/>
	<h2>${customer }</h2>
</body>
</html>