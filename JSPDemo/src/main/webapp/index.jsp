<%-- Directives:
	1) page
	2) include
	3) taglib
 --%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="java.util.function.Consumer"%>
<%@page import="java.time.LocalDate"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>

</head>
<body>
	<%@include file="header.html" %>
	
	
	<h3><a href='login.jsp'>Login Here</a></h3>
	
	<%
	
		List<String> fruits = new ArrayList();
		fruits.add("Mangoes");
		fruits.add("Apples");
		fruits.add("Grapes");
		session.setAttribute("fruits", fruits);
		response.sendRedirect("taglibdemo.jsp");
	%>
	
	
	<!--  HTML COmments -->
	<%-- JSP Comments --%>
	<%-- scriplets --%>
	<%!
	// declaration syntax
	public String message() {
		return "Hey There";
	}
	%>
	<%
	LocalDate date = LocalDate.now();
	// JSpWriter 
	out.println("<h1>Today's Date " + date + "</h1>");
	out.write("<h3>" + message() + "</h3>");
	out.println(request.getMethod());
	Consumer<String> consumer = (s) -> System.out.println("consumed " + s);
	consumer.accept("data");
	for (int i = 1; i <= 5; i++) {
		out.println("<p>" + i + "</p>");
	}
	%>
	
	<%-- JSP Expressions --%>
	<h3><%= message() %></h3>
	
</body>
</html>







