package com.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.model.Customer;
import com.model.LoginUser;

public class CustomerDB {

	public  boolean addCustomer(Customer customer, String password)
	{

		
		String sql = "insert into customer(cname, city, phone, email) values(?,?,?,?)";
		String sql1= "insert into loginuser values(?,?)";
		Connection con = MySQLComnnection.getConnection();
		try {
			con.setAutoCommit(false);
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		// try with resources
		try {
			PreparedStatement statement = con.prepareStatement(sql);
			statement.setString(1, customer.getCname());
			statement.setString(2, customer.getCity());
			statement.setString(3, customer.getPhone());
			statement.setString(4, customer.getEmail());
			statement.execute();
			
			PreparedStatement statement1 = con.prepareStatement(sql1);
			statement1.setString(1, customer.getEmail());
			statement1.setString(2, password);
			
			statement1.execute();
			con.commit();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			try {
				con.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			return false;
		}
		return true;	
	}
	public  boolean updateCustomer(Customer customer)
	{

		
		String sql = "update customer set cname=?, city = ?, phone = ? where cid=?";
		
		Connection con = MySQLComnnection.getConnection();
		
		try {
			PreparedStatement statement = con.prepareStatement(sql);
			statement.setString(1, customer.getCname());
			statement.setString(2, customer.getCity());
			statement.setString(3, customer.getPhone());
			statement.setInt(4, customer.getCid());
			statement.execute();
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
			return false;
		}
		return true;	
	}
	public  boolean deleteCustomer(int cid)
	{
		String sql = "delete from customer where cid=?";
		Connection con = MySQLComnnection.getConnection();
		try {
			
			PreparedStatement statement = con.prepareStatement(sql);
			statement.setInt(1, cid);
			// kindly delete from loginuser table as well
			statement.execute();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		return true;	
	}
	public List<Customer> getAllCustomers()
	{
		List<Customer> customers = new ArrayList<Customer>();
		Connection con = MySQLComnnection.getConnection();
		String sql = "select * from customer";
		try {
			PreparedStatement st = con.prepareStatement(sql);
			ResultSet rs = st.executeQuery();
			while(rs.next())
			{
				Customer customer = new Customer();
				customer.setCid(rs.getInt(1));
				customer.setCname(rs.getString(2));
				customer.setCity(rs.getString(3));
				customer.setPhone(rs.getString(4));
				customer.setEmail(rs.getString(5));
				customers.add(customer);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			try {
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return customers;
		
	}
	public Customer getCustomerByEmail(String email)
	{
		
		Connection con = MySQLComnnection.getConnection();
		String sql = "select * from customer where email=?";
		try {
			PreparedStatement st = con.prepareStatement(sql);
			st.setString(1, email);
			ResultSet rs = st.executeQuery();
			if(rs.next())
			{
				Customer customer = new Customer();
				customer.setCid(rs.getInt(1));
				customer.setCname(rs.getString(2));
				customer.setCity(rs.getString(3));
				customer.setPhone(rs.getString(4));
				customer.setEmail(rs.getString(5));
				return customer;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			try {
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
		
	}
	public Customer getCustomerById(int id)
	{
		
		Connection con = MySQLComnnection.getConnection();
		String sql = "select * from customer where cid=?";
		try {
			PreparedStatement st = con.prepareStatement(sql);
			st.setInt(1, id);
			ResultSet rs = st.executeQuery();
			if(rs.next())
			{
				Customer customer = new Customer();
				customer.setCid(rs.getInt(1));
				customer.setCname(rs.getString(2));
				customer.setCity(rs.getString(3));
				customer.setPhone(rs.getString(4));
				customer.setEmail(rs.getString(5));
				return customer;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			try {
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
		
	}
}
