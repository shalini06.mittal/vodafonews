package com.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.db.CustomerDB;
import com.db.LoginUserDB;
import com.model.Customer;
import com.model.LoginUser;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/login")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("login.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String email = request.getParameter("email");
		String pwd = request.getParameter("password");
		CustomerDB db = new CustomerDB();
		LoginUserDB logindb = new LoginUserDB();
		String role = logindb.validateUser(new LoginUser(email, pwd, null));
		if(role != null)
		{
			HttpSession session = request.getSession();
			session.setAttribute("email", email);
			if(role.equals("admin"))
			{
				response.sendRedirect("admin");
			}		
			else {
				Customer customer  = db.getCustomerByEmail(email);
				session.setAttribute("customer", customer);
				response.sendRedirect("dashboard.jsp");
			}
		}
		else {
			response.sendRedirect("login.jsp?error=Invalid Credentials");
		}
		
	}

}
